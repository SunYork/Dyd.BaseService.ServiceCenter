﻿using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Dyd.BaseService.ServiceCenter.Domain.Model
{
    //tb_error
    public class tb_error
    {

        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// serviceid
        /// </summary>
        [Display(Name = "服务Id")]
        public int serviceid { get; set; }
        /// <summary>
        /// 日志类型:1=服务端,2=客户端,3=系统
        /// </summary>
        [Display(Name = "日志类型")]
        public int logtype { get; set; }
        /// <summary>
        /// 消息内容
        /// </summary>
        [Display(Name = "消息内容")]
        public string msg { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }

        [Display(Name = "服务")]
        public string servicename { get; set; }

    }

    //tb_error
    public class tb_error_search : BaseSearch
    {
        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// serviceid
        /// </summary>
        [Display(Name = "serviceid")]
        public int serviceid { get; set; }
        /// <summary>
        /// 日志类型:1=服务端,2=客户端,3=系统
        /// </summary>
        [Display(Name = "日志类型:1=服务端,2=客户端,3=系统")]
        public int logtype { get; set; }
        /// <summary>
        /// 消息内容
        /// </summary>
        [Display(Name = "消息内容")]
        public string msg { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }

    }
}