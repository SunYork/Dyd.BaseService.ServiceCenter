﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using XXF.BaseService.ServiceCenter.Service.OpenDoc;

namespace XXF.BaseService.ServiceCenter.Service.Protocol
{
    /// <summary>
    /// 方法协议
    /// </summary>
    public class MethodProtocol
    {
        [Description("方法名")]
        public string Name { get; set; }
        [Description("输入参数列表")]
        public List<ParamProtocal> InputParams { get; set; }
        [Description("返回值参数")]
        public ParamProtocal ReturnParam { get; set; }
        [Description("方法文档")]
        public MethodDocAttribute MethodDoc { get; set; }
    }
    /// <summary>
    /// 参数协议
    /// </summary>
    public class ParamProtocal
    {
        [Description("参数名")]
        public string Name {get;set;}
        [Description("参数类型名")]
        public string TypeName {get;set;}
        [Description("是否是自定义类型")]
        public bool IsCustomType { get; set; }
        [JsonIgnore][ScriptIgnore]
        public Type SourceType { get; set; }
    }
    /// <summary>
    /// 实体类协议
    /// </summary>
    public class EntityProtocal
    {
        [Description("实体类名")]
        public string Name { get; set; }
        [Description("实体文档")]
        public EntityDocAttribute EntityDoc { get; set; }
        [Description("属性参数列表")]
        public List<ParamProtocal> PropertyParams { get; set; }
        [Description("属性参数文档")]
        public List<PropertyDocAttribute> PropertyDocs { get; set; }
    }
    /// <summary>
    /// 服务协议
    /// </summary>
    public class ServiceProtocal
    {
        [Description("服务名")]
        public string Name { get; set; }
        [Description("服务文档")]
        public ServiceDocAttribute ServiceDoc { get; set; }
        [Description("服务方法列表")]
        public List<MethodProtocol> MethodProtocols { get; set; }
        [Description("实体相关列表")]
        public List<EntityProtocal> EntityProtocals { get; set; }
    }
}
