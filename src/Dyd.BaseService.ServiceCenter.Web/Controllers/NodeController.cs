﻿using Dyd.BaseService.ServiceCenter.Domain;
using Dyd.BaseService.ServiceCenter.Domain.Bll;
using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using XXF.BaseService.ServiceCenter.Redis;
using XXF.BaseService.ServiceCenter.Service.Provider;
using XXF.BaseService.ServiceCenter.SystemRuntime;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Web.Controllers
{
    [Authorize]
    public class NodeController : Controller
    {
        /// <summary>
        /// 更新节点权重值
        /// </summary>
        /// <param name="id"></param>
        /// <param name="boostpercent"></param>
        /// <returns></returns>
        public ActionResult UpdateNodeBoostPercent(int id, int boostpercent)
        {
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    tb_node_bll.Instance.UpdateNodeBoostPercent(conn, id, boostpercent);
                    return Json(new { Flag = true });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }

        #region List
        /// <summary>
        /// 
        /// </summary>
        /// <param name="search">查询实体</param>
        /// <param name="pno">页码</param>
        /// <param name="pagesize">页大小</param>
        /// <returns></returns>
        public ActionResult NodeIndex(tb_node_search search)
        {
            int total = 0;
            IList<tb_node> list = new List<tb_node>();
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                IList<SelectListItem> selectList = new List<SelectListItem>();
                var serviceList = tb_service_bll.Instance.GetPageList(conn, new tb_service_search(), out total);
                foreach (var item in serviceList)
                {
                    selectList.Add(new SelectListItem { Value = item.id.ToString(), Text = item.servicename });
                }
                foreach (var item in selectList)
                {
                    if (search.serviceid.ToString() == item.Value)
                    {
                        item.Selected = true;
                    }
                }
                ViewBag.ServiceList = selectList;

                list = tb_node_bll.Instance.GetPageList(conn, search, out total);
                var pagelist = new SPagedList<tb_node>(list, search.Pno, search.PageSize, total);

                if (Request.IsAjaxRequest())
                {
                    return PartialView("_NodeIndex", pagelist);
                }
                else
                {
                    return View(pagelist);
                }
            }
        }
        #endregion

        #region Create
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public ActionResult NodeIndexCreate()
        {
            return View();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NodeIndexCreate(tb_node model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    if (tb_node_bll.Instance.IsExists(conn, model))
                    {
                        return Json(new { Flag = false, Message = "！" });
                    }
                    tb_node_bll.Instance.Add(conn, model);
                    return Json(new { Flag = true, Message = "添加成功！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }

        #endregion

        #region Edit

        /// <summary>
        /// 修改配置
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public ActionResult NodeIndexEdit(int id)
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                var model = tb_node_bll.Instance.Get(conn, id);
                return View(model);
            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NodeIndexEdit(tb_node model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    if (tb_node_bll.Instance.IsExists(conn, model))
                    {
                        return Json(new { Flag = false, Message = "！" });
                    }
                    tb_node_bll.Instance.Update(conn, model);
                    return Json(new { Flag = true, Message = "！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }

        #endregion

        #region Delete
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NodeIndexDelete(int id)
        {
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    tb_node_bll.Instance.Delete(conn, id);
                    return Json(new { Flag = true, Message = "删除成功！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }
        #endregion

        /// <summary>
        /// 节点协议
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public ActionResult NodeProtocalJson(int id)
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                var model = tb_node_bll.Instance.Get(conn, id);
                return View(model);
            }
        }


        /// <summary>
        /// 重启节点
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RestartNode(int id)
        {
            try
            {
                string serviceNamespace = string.Empty;
                int serviceId = 0;
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    var node = tb_node_bll.Instance.Get(conn, id);
                    if (null != node)
                    {
                        var service = tb_service_bll.Instance.Get(conn, node.serviceid);
                        if (null != service)
                        {
                            serviceNamespace = service.servicenamespace;
                            serviceId = service.id;
                        }
                    }
                }
                new RedisNetCommand(SystemConfigHelper.RedisServer).SendMessage(new ServiceNetCommand
                {
                    CommandType = EnumServiceCommandType.ServiceRestart,
                    ServiceID =serviceId,
                    NodeId = id,
                    ServiceNameSpace = serviceNamespace
                });
                return Json(new { Flag = true, Message = "重启节点成功！" });
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }
    }
}